use axum::routing::{get, post};
use axum::{extract::Form, response::Html, Router};

use std::net::SocketAddr;
use tower_http::services::ServeDir;

#[derive(serde::Deserialize)]
struct FormData {
    dissb: String,
}

async fn index() -> Html<&'static str> {
    Html(include_str!("templates/index.html"))
}

async fn result(form: Form<FormData>) -> Html<String> {
    let urn = generate_urn_with_checksum(&form.dissb);
    Html(format!(include_str!("templates/result.html"), urn = urn))
}

fn generate_urn_with_checksum(n: &str) -> String {
    let n = n.trim();
    let urn = format!("urn:nbn:ch:bel-bau-diss{}", n);
    match calculate_checksum(&urn) {
        Ok(checksum) => format!("{}{}", urn, checksum),
        Err(_) => format!("Ungültige Nummer: {}", n),
    }
}

fn calculate_checksum(urn: &str) -> Result<u8, std::fmt::Error> {
    let lookup = &LOOKUP_HASH_MAP;

    // Dokumentation des Algorithmus zur Berechnung der URN-Prüfziffer:
    // https://github.com/bohnelang/URN-Pruefziffer
    // resolver: https://nbn-resolving.org/

    // Zunaechst wird der URN-String in eine Ziffernfolge konvertiert, bei der
    // jedem Element der URN ein Zahlenwert zugewiesen wird. Die entsprechenden
    // Ziffern gehen aus der Konkordanztabelle hervor.
    let tmp: String = urn
        .chars()
        .filter_map(|c| lookup.get(&c.to_ascii_uppercase()).cloned())
        .collect();

    // Jede Zahl der Ziffernfolge wird einzeln von links nach rechts aufsteigend,
    // beginnend mit 1 multipliziert, anschliessend wird die Summe gebildet.
    let check: u32 = tmp
        .chars()
        .enumerate()
        .map(|(i, c)| c.to_digit(10).unwrap_or(0) * (i as u32 + 1))
        .sum();

    // Diese Produktsumme wird durch die letzte Zahl der URN-Ziffernfolge dividiert.
    // Die letzte Zahl vor dem Komma des Quotienten ist die Prüfziffer.
    let last_digit = tmp.chars().last()
        .and_then(|c| c.to_digit(10));

    match last_digit {
        Some(last_digit) => {
            let division_result = check as f32 / last_digit as f32;
            let check_digit = division_result % 10.0;
            return Ok(check_digit as u8);
        },
        None => Err(std::fmt::Error),
    }
}

const LOOKUP_HASH_MAP: phf::Map<char, &str> = phf::phf_map! {
    '0' => "1",
    '1' => "2",
    '2' => "3",
    '3' => "4",
    '4' => "5",
    '5' => "6",
    '6' => "7",
    '7' => "8",
    '8' => "9",
    '9' => "41",
    'A' => "18",
    'B' => "14",
    'C' => "19",
    'D' => "15",
    'E' => "16",
    'F' => "21",
    'G' => "22",
    'H' => "23",
    'I' => "24",
    'J' => "25",
    'K' => "42",
    'L' => "26",
    'M' => "27",
    'N' => "13",
    'O' => "28",
    'P' => "29",
    'Q' => "31",
    'R' => "12",
    'S' => "32",
    'T' => "33",
    'U' => "11",
    'V' => "34",
    'W' => "35",
    'X' => "36",
    'Y' => "37",
    'Z' => "38",
    ':' => "17",
    '-' => "39",
    '_' => "43",
    '/' => "45",
    '.' => "47",
    '+' => "49",
};

#[tokio::main]
async fn main() {
    let service = ServeDir::new("static");

    let app = Router::new()
        .route("/", get(index))
        .route("/", post(result))
        .nest_service("/static", service);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3030));

    let listener = tokio::net::TcpListener::bind(&addr)
        .await
        .expect("failed to bind to address. Is port already in use?");
    axum::serve(listener, app).await.expect("server failed");
}
