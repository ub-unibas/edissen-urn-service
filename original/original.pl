#!/usr/bin/perl

# generiere eine URN mit Pruefziffer

# Info:
#   http://www.persistent-identifier.de/?link=316
#
# Vgl:
#   http://nbn-resolving.de/nbnpruefziffer.php
#
# History;
#   03.11.2009 ava
#   17.11.2014 ignoriere nicht-numerische Eingabe / ava
#   02.05.2023 GUI Anpassungen; alphanumerische Eingabe wieder zulassen (z.B. "ep89223" für e-only E-Dissen ohne Diss-B Signatur) / dob

use strict;
use CGI qw/-no_xhtml/;

my $q = CGI->new;
print_header();
if ( $q->param('dissb') ) {
    print_result();
}
print_footer();

#---------------------------------
sub print_header {
    my $url = $q->url;
    print $q->header;
    print<<EOD;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>UB Basel: URN/DOI fuer E-Diss</title>
<style type="text/css">
body {
  font-family:sans-serif;
  font-size:80%;
}
div.kopf {
  background-color:#C3E4F1;
  padding:4;
  font-weight:bold;
}
div.feld {
  border: 1px solid #008;
  padding: 2mm;
  background-color: #f0f8ff;
  margin-left: 10mm;
  font-family:monospace;
  font-size:1.5em;
}
div.feldkommentar {
  margin-top: 2mm;
  margin-left: 10mm;
}
hr {
   border: 0;
   height: 1px;
   color:#000;
   background-color:#000;
}
</style>
<script type="text/javascript">
function toggleinfo() {
    oMore = document.getElementById("mehrinfo");
    oButt = document.getElementById("mehrbutton");
    if ( oMore.style.display == 'block' ) {
        oMore.style.display = 'none';
        oButt.value='mehr Info...';
    } else {
        oMore.style.display = 'block';
        oButt.value='schliessen...';
    }
}
</script>
</head>
<body>
<div class="kopf">UB Basel. Informatik</div>
<p><big><big>URN f&uuml;r E-Dissen generieren</big></big></p>

<form name="myform" method="post" action="$url">
DissB: <input name="dissb" type="text" size="10">
</form>

<script type="text/javascript">
document.myform.dissb.focus();
</script>

<hr>
<p>Anleitung: DissB-Nummer (oder, f&uuml;r e-only E-Dissertationen, EPrint-ID mit Vorspann "ep", z.B. "ep88293" f&uuml;r https://edoc.unibas.ch/88293/) eingeben und ENTER.<br>
Feldinhalt markieren, kopieren (Ctrl-C) und in edoc einf&uuml;gen (Ctrl-V ins Feld "Identification Number", "ID type" = "URN").</p>
<hr>
EOD
}
#---------------------------------
sub print_result {
    my $n = $q->param('dissb');

    #$n =~ s/\D//g; # löscht nicht-numerische Zeichen; auskommentiert 2023-05-02 / dob
    $n =~ s/^\s+//;
    $n =~ s/\s+$//;

    my $urn = 'urn:nbn:ch:bel-bau-diss' . $n;
    $urn = calculate_checksum($urn);

    print <<EOD;


<p>URN: </p>
<div class="feld">$urn</div>

EOD
}
#---------------------------------
sub print_footer {
    print<<EOD;
<br>
<form action="#" method="get">
<input id="mehrbutton" type="button" onClick="toggleinfo();" value="mehr...">
</form>

<div id="mehrinfo" style="display:none">

<p><b>urn</b> (uniform resource name): diese URN wird mit 90-t&auml;giger
Versp&auml;tung von der SNB geharvestet und bei der DNB registriert.<br>
Resolver: <a href="http://nbn-resolving.org/">http://nbn-resolving.org/</a></p>

<p><b>doi</b> (digital object identifier): der edoc DOI wird automatisch vergeben und t&auml;glich vom ETH DOI-Desk geharvestet und bei https://datacite.org/ registriert.<br>
Resolver: <a href="http://dx.doi.org/">http://dx.doi.org/</a></p>

<p><b>edoc</b>: Link auf den Dokumentenserver der Uni Basel.<br>
<a href="http://edoc.unibas.ch/">http://edoc.unibas.ch/</a></p>
<hr>
</div>

<p><small>(c) 2014 UB Basel</small></p>
</body>
</html>
EOD

}

#---------------------------------
sub calculate_checksum {
#---------------------------------
    my $urn = shift;
    my $lookup=lookup_hash();
    local $_;

    # Zunaechst wird der URN-String in eine Ziffernfolge konvertiert, bei der
    # jedem Element der URN ein Zahlenwert zugewiesen wird. Die entsprechenden
    # Ziffern gehen aus der Konkordanztabelle hervor.
    my @a = split(//,$urn);
    my $tmp='';
    while ( @a ) {
        $_ = shift @a;
        $tmp .= $lookup->{uc($_)};
    }
    # Jede Zahl der Ziffernfolge wird einzeln von links nach rechts aufsteigend,
    # beginnend mit 1 multipliziert, anschließend wird die Summe gebildet.
    @a = split(//,$tmp);
    my $check=0;
    my $i=1;
    while ( @a ) {
        $_ = shift @a;
        $check += $i++ * ($_ + 0);
    }
    # Diese Produktsumme wird durch die letzte Zahl der URN-Ziffernfolge dividiert.
    # Die letzte Zahl vor dem Komma des Quotienten ist die Prüfziffer.
    $check = int($check/$_);
    $check = $check . '';
    $check =~ s/^.*(.)$/$1/;
    $urn .= $check;

    $urn;
}

sub lookup_hash {{
'0' => '1',
'1' => '2',
'2' => '3',
'3' => '4',
'4' => '5',
'5' => '6',
'6' => '7',
'7' => '8',
'8' => '9',
'9' => '41',
'A' => '18',
'B' => '14',
'C' => '19',
'D' => '15',
'E' => '16',
'F' => '21',
'G' => '22',
'H' => '23',
'I' => '24',
'J' => '25',
'K' => '42',
'L' => '26',
'M' => '27',
'N' => '13',
'O' => '28',
'P' => '29',
'Q' => '31',
'R' => '12',
'S' => '32',
'T' => '33',
'U' => '11',
'V' => '34',
'W' => '35',
'X' => '36',
'Y' => '37',
'Z' => '38',
':' => '17',
'-' => '39',
'_' => '43',
'/' => '45',
'.' => '47',
'+' => '49',
}};