# EDissen URN Checksum Service

Ein kleiner Service, der die Checksumme für die URN einer E-Diss basierend auf der DissB-Nummer, der edoc id, oder der UNIverse ID generiert.

Wurde entwickelt nach den Vorgaben aus dem originalen Perl Skript: `original/original.pl`.


## Build

```bash
docker build -t urn-checksum-service .
```
