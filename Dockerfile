FROM cr.gitlab.switch.ch/memoriav/memobase-2020/utilities/rust-with-upx:latest AS build

ARG APPNAME=edissen-urn
WORKDIR /buildenv
RUN USER=root cargo new -q --vcs none --bin $APPNAME
WORKDIR /buildenv/$APPNAME
COPY . .
RUN cargo build --release
RUN mkdir /build && mv ./target/release/$APPNAME /build/app

FROM scratch
WORKDIR /app
COPY --from=build /build/app app
COPY static static
ENTRYPOINT ["./app"]